import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'

import Feeds from '../Components/Feeds.js'
import Hero from '../Components/Hero.js'
import SocialFeed from '../Components/SocialFeed.js'
import Headlines from '../Components/Headlines.js'
// import { Offline, Online } from 'react-detect-offline';
import FacebookComments from '../Components/FacebookComments.js'

const HomeStyles = styled.div`
  .home-content {
    padding: 0 1rem;
    margin: 3rem auto;
    max-width: 940px;
  }

  .offline-message {
    font-size: 15px;
    font-weight: semi-bold;
    font-size: calc(100% + 0.5vw);
  }

  .comments {
      padding-top: 30px;
      max-width: 600px;
  }
`

const IndexPage = ( { data }) => (
  <HomeStyles>
  	<Hero event={data.allContentfulEvent.edges[0].node}></Hero>
  	<div className="home-content">
      <Headlines data={data}></Headlines>
      <div className="uk-grid-medium uk-child-width-1-1@s uk-child-width-1-2@m" data-uk-grid="true">
        <Feeds data={data}></Feeds>
        <SocialFeed className="feed"></SocialFeed>
      </div>
      <div className="uk-grid-medium uk-child-width-1-1" data-uk-grid="true">
        <div className="comments">You know about another Tech Events that should be featured here, have suggestions or anything else? Please leave a comment! </div>
        <FacebookComments></FacebookComments>
      </div>
  	</div>
  </HomeStyles>
)

export default IndexPage

export const query = graphql`
  query FeedsQuery {
    allContentfulEvent(sort: {fields: [startDate], order: ASC}) {
      edges {
        node {
          title
          description {
            description
          }
          whyAttend {
            whyAttend
          }
          startDate(formatString: "MMMM DD, YYYY")
          countryIso
          address
          url
          id
        }
      }
      totalCount
    }
  }
`
