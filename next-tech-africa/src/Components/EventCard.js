import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'
import ToggleDisplay from 'react-toggle-display';

import FlagIcon from './FlagIcon.js'

const EventCardStyles = styled.div`

  max-width: 600px;
  .clean {
    background-color: #CF3060;
    color: white;
  }

  .text {
    font-family: Arial;
    text-align: left;
    color: #3A3A3A; 
  }

  .title {
    font-family: Arial;
    font-size: 1.2rem;
    line-height: 1.2;
    font-weight: bold;
    color: #1D75DC; 
  }

  p {
    font-family: Arial;
    color: #3A3A3A;
  }

  .details {
    padding: 1rem 1rem 0  1rem;
  }

  .why {
    font-weight: 600;
    font-size: 1.1rem;
  }

  .flag-icon {
    float: right;
    padding-top: 40px;
  }
`

const LocationDivStyled = styled.div`
    color: black;
    font-weight: normal;

`

const DateDivStyled = styled.div`
    color: black; 
    font-weight: bold;
    line-height: 1.2;
`

const BottomStyledDiv = styled.div`
   padding: 1rem;
`

export default ({ event }) => {
  return (
    <EventCardStyles className="uk-card uk-card-default uk-box-shadow-small">
      <div className="details">
          <a className="title text" style={{display: "table-cell"}} href={event.url} target="_blank" rel="noopener">{event.title}</a>
      </div>
      <BottomStyledDiv>
        <ToggleDisplay hide={event.countryIso == null}>
          <FlagIcon code={event.countryIso} size="lg" />
        </ToggleDisplay>
        <div className="uk-flex uk-flex-column text">
            <LocationDivStyled>{event.address}</LocationDivStyled>
            <DateDivStyled>{event.startDate}</DateDivStyled>
        </div>
      </BottomStyledDiv>
    </EventCardStyles>
  )
};