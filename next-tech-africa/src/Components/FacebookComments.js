import React, { Component} from 'react';
import FacebookProvider, { Comments } from 'react-facebook';

export default class FacebookComments extends Component {
  render() {
    return (
      <FacebookProvider appId="816134778490827">
        <Comments href="https://www.facebook.com/AfricaTechConnect/" />
      </FacebookProvider>
    );
  }
}