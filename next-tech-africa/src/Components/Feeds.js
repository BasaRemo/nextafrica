import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'

import UIkit from 'uikit';
import groupBy from 'lodash/groupBy';

import EventCard from '../Components/EventCard.js'

const FeedsStyles = styled.div`
    
    width: 560px;
    .clean {
        background-color: #CF3060;
        color: white;
    }

    .upcoming {
        font-weight: 500;
        padding-top: 50px;
        margin-bottom: 0px;
    }

    h2 {
        padding-top: 30px;
    }
`

export default ({ data }) => {
    const groupByMonth = groupBy(data.allContentfulEvent.edges, function(item) {
      return item.node.startDate.substring(0,3); 
    });
    return (
        <FeedsStyles className="content">
            
            <h1 className="uk-heading upcoming">Upcoming Events </h1>
            {
                Object.keys(groupByMonth).map(key =>
                    <div key={key}>
                        <h2 className="uk-heading">{key}</h2>
                        <hr></hr>
                        <ul className="uk-grid-small uk-child-width-1-1@s uk-child-width-1-1@m uk-child-width-1-1@l uk-text-center" data-uk-grid="true">
                            { groupByMonth[key].map(({ node }) => (
                                <li key={node.id}>
                                    <EventCard event={node} ></EventCard>
                                </li>
                            )) }
                        </ul>
                    </div>
                )
            }

        </FeedsStyles>
    );
        
};

