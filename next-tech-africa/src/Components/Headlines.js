import React from 'react'
import styled from 'styled-components'

import InstagramEmbed from 'react-instagram-embed'
import TweetEmbed from 'react-tweet-embed'

const SocialStyles = styled.div`
  .clean {
    background-color: #CF3060;
  }

  h4 {
    margin-bottom:5px;
    font-weight: 500;
  }

    h3 {
        font-weight: 500;
    }

    @media all and (min-width: 620px) {
        padding-top: 50px;
    }
    .subtitle {
        text-align: left;
    }
`
export default () => (
  <SocialStyles>
    <h1 className="uk-heading">Headlines</h1>
    <hr></hr>
    <ul className="uk-grid-small uk-child-width-1-1@s uk-child-width-1-2@m uk-child-width-1-3@l uk-text-center" data-uk-grid="true">
        <li>
            <h4 className="uk-heading uk-text-left">Very cool insigths from Yemi 👌🏿 </h4>
            <TweetEmbed id='948499625660616704' />
        </li>
        <li>
            <h4 className="uk-heading uk-text-left"> Deadline is March 1, 2018</h4>
            <TweetEmbed id='948558179369091072' />
        </li>
        <li>
            <h4 className="uk-heading uk-text-left">The Afro Generation of Builders 🔨</h4>
            <TweetEmbed id='946431912557600769' />
        </li>
    </ul>
  </SocialStyles>
)
