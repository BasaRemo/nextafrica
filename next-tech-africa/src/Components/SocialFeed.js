import React from 'react'
import styled from 'styled-components'

import InstagramEmbed from 'react-instagram-embed'
import TweetEmbed from 'react-tweet-embed'

import FacebookPage from '../Components/FacebookPage.js'

const SocialStyles = styled.div`
    max-width: 370px;

  h4 {
    margin-bottom:5px;
    font-weight: 500;
  }

    h2 {
        font-weight: 500;
    }

    @media all and (min-width: 620px) {
        padding-top: 59px;
    }
    .subtitle {
        text-align: left;
        padding-bottom: 5px;
    }
`
export default () => (
  <SocialStyles>
    <h2 className="uk-heading">What's happening?</h2>
    <hr></hr>
    <ul className="uk-grid-medium uk-child-width-1-1@s uk-text-center" data-uk-grid="true">
        <li>
            <h4 className="uk-heading uk-text-left">It's about owning our narratives  👍🏽</h4>
            <div className="uk-card">
            <InstagramEmbed url='https://instagr.am/p/BaD7LohHnN2/' hideCaption={false} containerTagName='div'/>
            </div>
        </li>
        <li>
            <h4 className="uk-heading uk-text-left">AfroTech 2017 was sure dope</h4>
            <div className="subtitle">This website litteraly comes from watching folks doing their things that weekend.</div>
            <div className="uk-card">
            <InstagramEmbed url='https://instagr.am/p/BbYl-nBHiBk/' hideCaption={true} containerTagName='div'/>
            </div>
        </li>
        <li>
            <h4 className="uk-heading uk-text-left">Don't miss you shot..Apply to YC!</h4>
            <div className="uk-card">
            <InstagramEmbed url='https://instagr.am/p/BYBt87kFA24/' hideCaption={true} containerTagName='div'/>
            </div>
        </li>
        <li>
            <h4 className="uk-heading uk-text-left">Afrobytes 2017 looked 🔥</h4>
            <TweetEmbed id='944576371254353920' />
        </li>
        <li>
            <h4 className="uk-heading uk-text-left">Motivation</h4>
            <TweetEmbed id='945356868330934272' />
        </li>
        <li>
            <h4 className="uk-heading uk-text-left">Like Our Tech Safari on Facebook! ⛵</h4>
            <div className="subtitle">We share our progress building this project and the amazing stuffs happening in the Afro Tech</div>
            <FacebookPage></FacebookPage>
        </li>
    </ul>
  </SocialStyles>
)
