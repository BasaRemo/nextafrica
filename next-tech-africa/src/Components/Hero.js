import React from 'react'
import styled from 'styled-components'

import logoImg from '../images/background.webp'
import EventCard from '../Components/EventCard.js'

const HeroStyles = styled.div`
  background: #1656A0;
  padding: 120px 20px 40px 20px; 
  margin-top: -48px;

  @media screen and (max-width:450px) {
       padding-top: 70px;
  }

  .next-event {
    padding-left: 10px;
    padding-top: 5px;
  }

  .main-title {
    font-size: 50px;
    font-weight: bold;
    font-size: calc(100% + 3vw);
    color: #FFBA49;
  }

  .main-subtitle {
    font-size: 30px;
    font-weight: bold;
    font-size: calc(100% + 1vw);
    padding-bottom: 5rem;
    margin-top: 5px;
    max-width: 800px;
  }

  .copy {
    color: white;
  }

  .annonce {
    font-size: 15px;
    font-weight: semi-bold;
    font-size: calc(100% + 0.5vw);
  }
`

export default ({ event }) => (
  <HeroStyles className="header-image">
    <div className="uk-flex uk-flex-center@m">
      <div className="uk-flex-column">
        <h1 className="main-title"> Where African Tech Connects</h1>
        <h2 className="copy main-subtitle"> We follow the Top Tech Events happening in Africa and the African diaspora</h2>
        <span className="copy annonce">  Ongoing Featured Event : </span>
        <div className="next-event uk-grid-small uk-child-width-1-1@m " data-uk-grid="true">
          <EventCard event={event} ></EventCard>
        </div>
      </div>
    </div>
  </HeroStyles>
)
