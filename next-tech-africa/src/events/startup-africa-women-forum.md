---
title: 2018 STARTUPAFRICA WOMEN ENTREPRENEURSHIP FORUM
date: "2018-02-22"
website: http://startupafrica.org/swef/
---

## ABOUT SWEF

#### STARTUPAFRICA WOMEN ENTREPRENEURSHIP FORUM

* * *

At StartUpAfrica, we believe that women are the backbone of the African society as seen from village markets to corporate board rooms where they are creating an entrepreneurship movement. Their exposure to entrepreneurship and self-employment is changing the social mindset on the benefits of business and success.

The 2018 StartUpAfrica Women Entrepreneurship Forum is issues-focused and is for executives of small and medium enterprises, professionals, and aspiring entrepreneurs. The forum will be held on February 22-23, 2018 at Silver Palms Resort and Spa in Kilifi, Kenya.

With an expected attendance of over 200 women from Kenya and beyond, SWEF’s program will cover relevant topics of interest to the women SME owners and other participants who want to get into business.

The forum’s agenda will include keynote addresses and offer skills training, panels that will offer knowledge on various business opportunities, best practices in doing business in Africa and internationally, access to capital, strategies for driving innovation, and, setting up and running replicable, scalable and sustainable businesses, all centered around running business successfully and addressing issues that entrepreneurs deal with.

SWEF 2018 like past forums is meant to educate, connect and empower female entrepreneurs to start, grow and sustain their enterprises in a competitive environment that will lead towards a direct impact on the economy as well as the entrepreneurial ecosystem.

See you there!

## AGENDA

Day 1, February 22, 2018

4:00 pm    Welcome Remarks

4:15 pm     Roundtable: The Power of Your Social Media Groups

5:00 pm    Roundtable: Marketing and Branding Your Small Business

7:00 pm    NETWORKING DINNER

Day 2, February 23, 2018

9:00am   Welcome Remarks

9:15am      Keynote

9:45 am    PANEL: Access to capital for women entrepreneurs

10:30 am    MORNING COFFEE/TEA AND NETWORKING BREAK   

11:00 am    PANEL: Skills and capacity building

11:45 am    PANEL: Innovation and technology in business

12:30 PM    NETWORKING LUNCH

1:30 pm    Turning your passion into a viable business

1:50 pm    PANEL:    Best practices in doing business in Africa and beyond

2:35 PM    PANEL:    Setting up and Running Sustainable Business: What you need to know

3:20 pm    AFTERNOON COFFEE/TEA AND NETWORKING BREAK

3:45 pm    PANEL: Government and Women Entrepreneurs: Opportunities and Support

## SPONSORS

##### EXHIBIT

The StartUpAfrica Women Entrepreneurship Forum is issues-focused and is for executives of small and medium enterprises, professionals, and aspiring entrepreneurs. We provide a platform for entrepreneurs to meet and share information on the challenges and opportunities in the industry.

- Two (2) complimentary delegate places for company representatives
- 6 foot table and 2 chairs
- Refreshments and lunch
- Networking opportunities with delegates
- WiFi
- Electrical access